import argparse
import os
import ndjson
import sys
from jobtech.anonymisering import unionrep_anonymization


def read_ads_stream_from_std_in():
    reader = ndjson.reader(sys.stdin)
    for ad in reader:
        yield ad


def read_ads_from_file(rel_filepath):
    # Open input file with ads and load ndjson to dictionary...
    ads_path = currentdir + rel_filepath
    with open(ads_path, encoding="utf8") as ads_file:
        ads = ndjson.load(ads_file)
    return ads


def print_ad(ad):
    output_json = ndjson.dumps([ad], ensure_ascii=False)
    print(output_json, file=sys.stdout)

def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def str_to_bool(s):
    if s and (s.lower() == 'true' or s.lower() == 'yes'):
        return True
    else:
        return False


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Removes sentences that mentions union representatives in the ad description')
    parser.add_argument('--filepath', help='Optional relative filepath to read ads from', required=False)
    parser.add_argument('--debug', help='Optional flag if debug information should be printed', required=False)

    args = parser.parse_args()

    filepath = args.filepath
    debug = str_to_bool(args.debug)


    global currentdir
    currentdir = os.path.dirname(os.path.realpath(__file__)) + os.sep

    unionrep_anonymizer = unionrep_anonymization.unionrep_anonymize_swe()
    unionrep_anonymizer.initialize()

    if filepath:
        ads = read_ads_from_file(filepath)
    else:
        ads = read_ads_stream_from_std_in()

    removed_sentences = []

    for ad in ads:
        try:
            if 'originalJobPosting' in ad and 'description' in ad['originalJobPosting']:

                ad_description = ad['originalJobPosting']['description']
                ad_description = ad_description.replace('.', '. ')

                anonymize_result = unionrep_anonymizer.anonymizeText(ad_description, extraKeywords=None, ignoredSentences=None)
                cleaned_ad_description = anonymize_result[0]
                if len(anonymize_result[1]) > 0:
                    removed_sentences.append('\n'.join(anonymize_result[1]))
                ad['originalJobPosting'].update(description=cleaned_ad_description)

            print_ad(ad)

        except:
            e = sys.exc_info()[0]
            print_error(e)


    if debug:
        print('Removed sentences:')
        for removed in removed_sentences:
            print(removed)
            print('=' * 40)