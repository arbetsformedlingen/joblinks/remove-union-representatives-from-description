FROM docker.io/library/python:3.12.6-slim-bookworm AS base

WORKDIR /app

COPY requirements.txt requirements.txt
COPY . /app

RUN apt-get -y update && apt-get install -y git && pip install -r requirements.txt &&\
# Download and install nltk.punkt-tokenizer
    pip install nltk==3.8.1 && \
    python -c "import nltk; nltk.download('punkt', download_dir='/usr/share/nltk_data')"


###############################################################################
FROM base AS test

COPY tests/testdata/ /testdata/
RUN  python main.py < /testdata/input.10 > /tmp/output.10 \
     && diff /testdata/expected_output.10 /tmp/output.10 \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

CMD ["python","main.py"]
